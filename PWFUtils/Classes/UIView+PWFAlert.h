//
//  UIView+PWFAlert.h
//  XKit
//
//  Created by pengweifeng on 2018/4/9.
//

#import <UIKit/UIKit.h>

@interface UIView (PWFAlert)

/**
 当前View上弹文本框
 
 @param text 弹框上显示的文本
 */
- (void)pwf_showAlert:(NSString *)text;

/**
 当前View上弹文本框, delay秒之后消失
 
 @param text 弹框上显示的文本
 @param delay 显示时间(单位秒)
 */
- (void)pwf_showAlert:(NSString *)text hideAfterDelay:(NSUInteger)delay;

/**
 app主window上显示文本
 
 @param text 弹框上显示的文本
 */
+ (void)pwf_showAlert:(NSString *)text;

/**
 app主window上显示文本, delay秒之后消失
 
 @param text 弹框上显示的文本
 @param delay 显示时间(单位秒)
 */
+ (void)pwf_showAlert:(NSString *)text hideAfterDelay:(NSUInteger)delay;
@end
