//
//  UITextField+PhoneNumber.h
//  ELPassport
//
//  Created by pengweifeng on 2018/4/18.
//  手机号码以3,4,4的格式分隔

#import <UIKit/UIKit.h>

@interface UITextField (PWFPhoneNumber)<UITextFieldDelegate>

/**
 获取删除空格后的字符串
 
 @return 删除空格后的字符串
 */
- (NSString *)trimmedText;

@end
