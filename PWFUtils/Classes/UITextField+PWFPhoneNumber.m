//
//  UITextField+PhoneNumber.m
//  ELPassport
//
//  Created by pengweifeng on 2018/4/18.
//

#import "UITextField+PWFPhoneNumber.h"

@implementation UITextField (PWFPhoneNumber)

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length == 13 && range.length == 0) {
        return NO;
    }
    //如果输入的字符不是数字,则文本不发生改变
    NSString *withoutDecimalStr = [string stringByTrimmingCharactersInSet:[NSCharacterSet decimalDigitCharacterSet]];
    if (withoutDecimalStr.length > 0) {
        return NO;
    }
    
    NSMutableString *mStr = [NSMutableString stringWithString:textField.text];
    // 删减字符
    if(string.length == 0 && range.location < textField.text.length) {
        NSString *removeTemp = [textField.text substringWithRange:NSMakeRange(range.location, range.length)];
        NSString *removeTempFontier = @"";
        if(range.location >= 1) {
            removeTempFontier = [textField.text substringWithRange:NSMakeRange(range.location - 1, range.length)];
        }
        
        if(![removeTemp isEqualToString:@" "]) {
            [mStr deleteCharactersInRange:NSMakeRange(range.location, range.length)];
            NSMutableString *tempMutableStr = [NSMutableString stringWithString:[self delSpaceAndNewline:mStr]];
            
            if (tempMutableStr.length >= 4) {
                [tempMutableStr insertString:@" " atIndex:3];
            }
            
            if(tempMutableStr.length >= 9) {
                [tempMutableStr insertString:@" " atIndex:8];
            }
            [textField setText:tempMutableStr];
        }
        
        // 判断当前位置往前一个字符是否为空格
        if ([removeTempFontier isEqualToString:@" "]) {
            [self setTextRangeWithOffset:range.location - 1];
        } else {
            [self setTextRangeWithOffset:range.location];
        }
        
        return NO;
    }
    // 输入字符
    if(string.length > 0) {
        [mStr deleteCharactersInRange:NSMakeRange(range.location, range.length)];
        NSUInteger location = range.location + 1;
        if(range.location == 3 || range.location == 8) {
            location += 1;
        }
        [mStr insertString:string atIndex:range.location];
        // 每次输入都先清除空格
        NSMutableString *noBlankString = [NSMutableString stringWithString:[self delSpaceAndNewline:mStr]];
        // 插入空格
        if (noBlankString.length >= 4 && noBlankString.length <= 7) {
            [noBlankString insertString:@" " atIndex:3];
        } else if (noBlankString.length > 7) {
            [noBlankString insertString:@" " atIndex:3];
            [noBlankString insertString:@" " atIndex:8];
        }
        
        [textField setText:noBlankString];
        [self setTextRangeWithOffset:location];
        return NO;
    }
    
    return YES;
}

/**
 设置光标位置

 @param offset 光标位置
 */
- (void)setTextRangeWithOffset:(NSUInteger)offset
{
    UITextPosition* beginning = self.beginningOfDocument;
    UITextPosition* startPosition = [self positionFromPosition:beginning offset:offset];
    UITextPosition* endPosition = [self positionFromPosition:beginning offset:offset];
    UITextRange* selectionRange = [self textRangeFromPosition:startPosition toPosition:endPosition];
    [self setSelectedTextRange:selectionRange];
}

- (NSString *)delSpaceAndNewline:(NSString *)string
{
    NSMutableString *mutStr = [NSMutableString stringWithString:string];
    NSRange range = {0, mutStr.length};
    [mutStr replaceOccurrencesOfString:@" " withString:@"" options:NSLiteralSearch range:range];
    NSRange range2 = {0, mutStr.length};
    [mutStr replaceOccurrencesOfString:@"\n" withString:@"" options:NSLiteralSearch range:range2];
    
    return mutStr;
}

/**
 获取删除空格后的字符串

 @return 删除空格后的字符串
 */
- (NSString *)trimmedText
{
    return [self delSpaceAndNewline:self.text];
}
@end
