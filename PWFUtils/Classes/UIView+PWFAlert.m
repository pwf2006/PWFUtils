//
//  UIView+PWFAlert.m
//  XKit
//
//  Created by pengweifeng on 2018/4/9.
//

#import "UIView+PWFAlert.h"
#import <MBProgressHUD/MBProgressHUD.h>

@implementation UIView (PWFAlert)

/**
 当前View上弹文本框

 @param text 弹框上显示的文本
 */
- (void)pwf_showAlert:(NSString *)text
{
    [MBProgressHUD hideHUDForView:self animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.numberOfLines = 10;
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    hud.label.text = (text ?: @"");
}

/**
 当前View上弹文本框, delay秒之后消失

 @param text 弹框上显示的文本
 @param delay 显示时间(单位秒)
 */
- (void)pwf_showAlert:(NSString *)text hideAfterDelay:(NSUInteger)delay
{
    [MBProgressHUD hideHUDForView:self animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.numberOfLines = 10;
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    hud.label.text = (text ?: @"");
    [hud hideAnimated:YES afterDelay:delay];
}

/**
 app主window上显示文本

 @param text 弹框上显示的文本
 */
+ (void)pwf_showAlert:(NSString *)text
{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [MBProgressHUD hideHUDForView:keyWindow animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.numberOfLines = 10;
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    hud.label.text = (text ?: @"");
}

/**
 app主window上显示文本, delay秒之后消失

 @param text 弹框上显示的文本
 @param delay 显示时间(单位秒)
 */
+ (void)pwf_showAlert:(NSString *)text hideAfterDelay:(NSUInteger)delay
{
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    [MBProgressHUD hideHUDForView:keyWindow animated:YES];
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:keyWindow animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.label.numberOfLines = 10;
    hud.bezelView.style = MBProgressHUDBackgroundStyleSolidColor;
    hud.bezelView.backgroundColor = [UIColor blackColor];
    hud.contentColor = [UIColor whiteColor];
    hud.label.text = (text ?: @"");
    [hud hideAnimated:YES afterDelay:delay];
}
@end
