//
//  UIColor+WFUtils.m
//  WFUtils
//
//  Created by pengweifeng on 2017/6/2.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import "UIColor+PWFUtils.h"

@implementation UIColor (PWFUtils)

/**
 根据十六进制整数及不透明度算出对应的颜色

 @param hexValue 十六进制整数
 @param alphaValue 不透明度
 @return 对应的颜色对象
 */
+ (UIColor *)colorWithHex:(NSUInteger)hexValue alpha:(CGFloat)alphaValue
{
    CGFloat redValue = (float)((hexValue & 0xFF0000) >> 16) / 255.0;
    CGFloat greenValue = (float)((hexValue & 0xFF00) >> 8) / 255.0;
    CGFloat blueValue = (float)(hexValue & 0xFF) / 255.0;
    
    return [UIColor colorWithRed:redValue green:greenValue blue:blueValue alpha:alphaValue];
}

/**
 根据十六进制整数算出对应的颜色

 @param hexValue 十六进制整数
 @return 对应的颜色对象
 */
+ (UIColor *)colorWithHex:(NSUInteger)hexValue
{
    return [self colorWithHex:hexValue alpha:1.f];
}

/**
 根据十六进制字符串算出对应的颜色

 @param colorStr 十六进制字符串
 @return 对应的颜色对象
 */
+ (UIColor *)colorWithHexString:(NSString *)colorStr
{
    NSString *aColorStr = [[colorStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] uppercaseString];
    
    if (aColorStr.length < 6) {
        return [UIColor clearColor];
    }
    
    if ([aColorStr hasPrefix:@"0X"] == YES) {
        aColorStr = [aColorStr substringFromIndex:2];
    } else if ([aColorStr hasPrefix:@"#"] == YES) {
        aColorStr = [aColorStr substringFromIndex:1];
    }
    
    if (aColorStr.length < 6) {
        return [UIColor clearColor];
    }
    
    NSString *redStr = [aColorStr substringWithRange:NSMakeRange(0, 2)];
    NSString *greenStr = [aColorStr substringWithRange:NSMakeRange(2, 2)];
    NSString *blueStr = [aColorStr substringWithRange:NSMakeRange(4, 2)];
    
    unsigned int red, green, blue;
    [[NSScanner scannerWithString:redStr] scanHexInt:&red];
    [[NSScanner scannerWithString:greenStr] scanHexInt:&green];
    [[NSScanner scannerWithString:blueStr] scanHexInt:&blue];
    
    return [UIColor colorWithRed:(red / 255.f) green:(green / 255.f) blue:(blue / 255.f) alpha:1.f];
}

/**
 获取一个随机色

 @return 返回一个生成的随机色
 */
+ (UIColor *)randomColor {
    NSInteger aRedValue = arc4random() % 255;
    NSInteger aGreenValue = arc4random() % 255;
    NSInteger aBlueValue = arc4random() % 255;
    UIColor *randColor = [UIColor colorWithRed:aRedValue / 255.0f green:aGreenValue / 255.0f blue:aBlueValue / 255.0f alpha:1.0f];
    
    return randColor;
}
@end
