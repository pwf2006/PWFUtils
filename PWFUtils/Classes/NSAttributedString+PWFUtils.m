//
//  NSAttributedString+WFUtils.m
//  WFUtils
//
//  Created by pengweifeng on 2017/6/1.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import "NSAttributedString+PWFUtils.h"

@implementation NSAttributedString (PWFUtils)

/**
 根据宽度计算属性字符串的高度

 @param width 属性字符串要占用的宽度
 @return 属性字符串匹配后的高度
 */
- (CGFloat)heightForWidth:(CGFloat)width
{
    if (self.length == 0) {
        return 0.f;
    }
    
    CGSize size = CGSizeMake(width, CGFLOAT_MAX);
    CGSize adaptSize = [self boundingRectWithSize:size options:NSStringDrawingTruncatesLastVisibleLine |NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil].size;
    CGFloat height = ceil(adaptSize.height);
    
    return height;
}

@end
