//
//  UIButton+PWFUtils.h
//  Pods
//
//  Created by pengweifeng on 09/08/2017.
//
//

#import <UIKit/UIKit.h>

@interface UIButton (PWFUtils)

@property (nonatomic, assign) CGFloat minTouchWidth;

@property (nonatomic, assign) CGFloat minTouchHeight;

@end
