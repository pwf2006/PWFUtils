//
//  NSDictionary+PWFUtils.m
//  Pods
//
//  Created by pengweifeng on 2017/8/17.
//
//

#import "NSDictionary+PWFUtils.h"

@implementation NSDictionary (PWFUtils)

/**
 NSDictionary->json串
 
 @return json串
 */
- (NSString *)toString
{
    NSString *jsonString = nil;
    NSError *error;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted // Pass 0 if you don't care about the readability of the generated string
                                                         error:&error];
    if (!jsonData) {
        NSLog(@"Got an error: %@", error);
    } else {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
    return jsonString;
}

@end
