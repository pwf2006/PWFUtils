//
//  UIButton+PWFUtils.m
//  Pods
//
//  Created by pengweifeng on 09/08/2017.
//
//

#import "UIButton+PWFUtils.h"
#import <objc/runtime.h>

@implementation UIButton (PWFUtils)

- (CGFloat)minTouchWidth
{
    NSNumber *width = objc_getAssociatedObject(self, @selector(minTouchWidth));
    return [width floatValue];
}

- (void)setMinTouchWidth:(CGFloat)minWidth
{
    objc_setAssociatedObject(self, @selector(minTouchWidth), [NSNumber numberWithFloat:minWidth] , OBJC_ASSOCIATION_ASSIGN);
}

- (CGFloat)minTouchHeight
{
    NSNumber *height = objc_getAssociatedObject(self, @selector(minTouchWidth));
    return [height floatValue];
}

- (void)setMinTouchHeight:(CGFloat)minHeight
{
    objc_setAssociatedObject(self, @selector(minTouchWidth), [NSNumber numberWithFloat:minHeight], OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)pointInside:(CGPoint)point withEvent:(nullable UIEvent *)event {
    
    return CGRectContainsPoint(HitExpandBound(self.bounds, self.minTouchWidth, self.minTouchHeight), point);
}

CGRect HitExpandBound(CGRect bounds, CGFloat minimumHitTestWidth, CGFloat minimumHitTestHeight)
{
    
    CGRect hitExpandBound = bounds;
    
    if (minimumHitTestWidth > bounds.size.width) {
        hitExpandBound.size.width = minimumHitTestWidth;
        hitExpandBound.origin.x -= (hitExpandBound.size.width - bounds.size.width)/2;
    }
    
    if (minimumHitTestHeight > bounds.size.height) {
        hitExpandBound.size.height = minimumHitTestHeight;
        hitExpandBound.origin.y -= (hitExpandBound.size.height - bounds.size.height)/2;
    }
    
    return hitExpandBound;
}

@end
