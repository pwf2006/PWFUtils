//
//  UIControl+PWFUtils.h
//  PWFUtils
//
//  Created by pengweifeng on 2017/9/30.
//  Copyright © 2017年 pwf2006. All rights reserved.
//
//  ** 控制控件的连续点击,通过设置点击间隔clickInterval **
//

#import <UIKit/UIKit.h>

@interface UIControl (PWFUtils)

/** 控件的点击时间间隔 */
@property (nonatomic, assign) NSTimeInterval clickInterval;
/** 控件是否可点击 */
@property (nonatomic, assign) BOOL canNotClick;

@end
