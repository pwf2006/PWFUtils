//
//  NSDictionary+PWFUtils.h
//  Pods
//
//  Created by pengweifeng on 2017/8/17.
//
//

#import <Foundation/Foundation.h>

@interface NSDictionary (PWFUtils)

/**
 NSDictionary->json串
 
 @return json串
 */
- (NSString *)toString;

@end
