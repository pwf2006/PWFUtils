//
//  WFUtils.m
//  WFUtils
//
//  Created by pengweifeng on 2017/6/1.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import "PWFUtil.h"
#import <sys/utsname.h>
#import <sys/mount.h>
#import <UIKit/UIKit.h>
#include <ifaddrs.h>
#include <net/if.h>
#import <mach/mach.h>

static NSString * const PWFUtilsWifiSndCount = @"WifiSndCount";
static NSString * const PWFUtilsWifiRcvCount = @"WifiRcvCount";
static NSString * const PWFUtilsWWanSndCount = @"WWanSndCount";
static NSString * const PWFUtilsWWanRcvCount = @"WWanRcvCount";

@implementation PWFUtil

/**
 获取UUID
 
 @return 返回生成的UUID
 */
+ (NSString *)UUID {
    NSString *result;
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] > 6.0) {
        result = [[NSUUID UUID] UUIDString];
    } else {
        CFUUIDRef uuidRef = CFUUIDCreate(NULL);
        CFStringRef uuid = CFUUIDCreateString(NULL, uuidRef);
        CFRelease(uuidRef);
        result = (__bridge_transfer NSString *)uuid;
    }
    
    return result;
}

/**
 获取设备信息

 @return 返回设备信息
 */
+ (NSString *)getDeviceInfo {
    struct utsname systemInfo;
    uname(&systemInfo);
    NSString *platform = [NSString stringWithCString:systemInfo.machine encoding:NSASCIIStringEncoding];
    if ([platform isEqualToString:@"iPhone1,1"]) return @"iPhone 2G";
    if ([platform isEqualToString:@"iPhone1,2"]) return @"iPhone 3G";
    if ([platform isEqualToString:@"iPhone2,1"]) return @"iPhone 3GS";
    if ([platform isEqualToString:@"iPhone3,1"]) return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,2"]) return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone3,3"]) return @"iPhone 4";
    if ([platform isEqualToString:@"iPhone4,1"]) return @"iPhone 4S";
    if ([platform isEqualToString:@"iPhone5,1"]) return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,2"]) return @"iPhone 5";
    if ([platform isEqualToString:@"iPhone5,3"]) return @"iPhone 5c";
    if ([platform isEqualToString:@"iPhone5,4"]) return @"iPhone 5c";
    if ([platform isEqualToString:@"iPhone6,1"]) return @"iPhone 5s";
    if ([platform isEqualToString:@"iPhone6,2"]) return @"iPhone 5s";
    if ([platform isEqualToString:@"iPhone7,1"]) return @"iPhone 6 Plus";
    if ([platform isEqualToString:@"iPhone7,2"]) return @"iPhone 6";
    if ([platform isEqualToString:@"iPhone8,1"]) return @"iPhone 6s";
    if ([platform isEqualToString:@"iPhone8,2"]) return @"iPhone 6s Plus";
    // 日行两款手机型号均为日本独占，可能使用索尼FeliCa支付方案而不是苹果支付
    if ([platform isEqualToString:@"iPhone9,1"])    return @"国行、日版、港行iPhone 7";
    if ([platform isEqualToString:@"iPhone9,2"])    return @"港行、国行iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone9,3"])    return @"美版、台版iPhone 7";
    if ([platform isEqualToString:@"iPhone9,4"])    return @"美版、台版iPhone 7 Plus";
    if ([platform isEqualToString:@"iPhone8,4"]) return @"iPhone SE";
    if ([platform isEqualToString:@"iPod1,1"]) return @"iPod Touch 1G";
    if ([platform isEqualToString:@"iPod2,1"]) return @"iPod Touch 2G";
    if ([platform isEqualToString:@"iPod3,1"]) return @"iPod Touch 3G";
    if ([platform isEqualToString:@"iPod4,1"]) return @"iPod Touch 4G";
    if ([platform isEqualToString:@"iPod5,1"]) return @"iPod Touch 5G";
    if ([platform isEqualToString:@"iPad1,1"]) return @"iPad 1G";
    if ([platform isEqualToString:@"iPad2,1"]) return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,2"]) return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,3"]) return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,4"]) return @"iPad 2";
    if ([platform isEqualToString:@"iPad2,5"]) return @"iPad Mini 1G";
    if ([platform isEqualToString:@"iPad2,6"]) return @"iPad Mini 1G";
    if ([platform isEqualToString:@"iPad2,7"]) return @"iPad Mini 1G";
    if ([platform isEqualToString:@"iPad3,1"]) return @"iPad 3";
    if ([platform isEqualToString:@"iPad3,2"]) return @"iPad 3";
    if ([platform isEqualToString:@"iPad3,3"]) return @"iPad 3";
    if ([platform isEqualToString:@"iPad3,4"]) return @"iPad 4";
    if ([platform isEqualToString:@"iPad3,5"]) return @"iPad 4";
    if ([platform isEqualToString:@"iPad3,6"]) return @"iPad 4";
    if ([platform isEqualToString:@"iPad4,1"]) return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,2"]) return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,3"]) return @"iPad Air";
    if ([platform isEqualToString:@"iPad4,4"]) return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,5"]) return @"iPad Mini 2G";
    if ([platform isEqualToString:@"iPad4,6"]) return @"iPad Mini 2G";
    if ([platform isEqualToString:@"i386"]) return @"iPhone Simulator";
    if ([platform isEqualToString:@"x86_64"]) return @"iPhone Simulator";
    
    return platform;
}

/**
 获取app名称

 @return 返回app名称
 */
+ (NSString *)appName {
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    
    return [infoDic objectForKey:@"CFBundleDisplayName"];
}

/**
 获取app版本号

 @return 返回app版本号
 */
+ (NSString *)appVersion {
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    
    return [infoDic objectForKey:@"CFBundleShortVersionString"];
}

/**
 获取app build号

 @return 返回app build号
 */
+ (NSString *)appBuidVersion {
    NSDictionary *infoDic = [[NSBundle mainBundle] infoDictionary];
    
    return [infoDic objectForKey:@"CFBundleShortVersionString"];
}

/**
 获取自上次开机以来wifi和wwan各自的收发总量(以B为单位)

 @return 包含wifi和wwan各自收发总量的字典
 */
+ (NSDictionary *)getMoblieDataTraffic {
    NSDictionary *dataCountDic;
    BOOL success;
    struct ifaddrs *addrs;
    const struct ifaddrs *cursor;
    const struct if_data *networkStatisc;
    
    NSUInteger wifiSnd = 0;;
    NSUInteger wifiRcv = 0;
    NSUInteger wwanSnd = 0;
    NSUInteger wwanRcv = 0;
    
    NSString *name;
    success = getifaddrs(&addrs) == 0;
    if (success) {
        cursor = addrs;
        while (cursor != NULL) {
            name=[NSString stringWithFormat:@"%s",cursor->ifa_name];
            
            if (cursor->ifa_addr->sa_family == AF_LINK) {
                //wifi消耗流量
                if ([name hasPrefix:@"en"]) {
                    networkStatisc = (const struct if_data *) cursor->ifa_data;
                    wifiSnd += networkStatisc->ifi_obytes;
                    wifiRcv += networkStatisc->ifi_ibytes;
                }
                
                //移动网络消耗流量
                if ([name hasPrefix:@"pdp_ip0"]) {
                    networkStatisc = (const struct if_data *) cursor->ifa_data;
                    wwanSnd += networkStatisc->ifi_obytes;
                    wwanRcv += networkStatisc->ifi_ibytes;
                }
            }
            cursor = cursor->ifa_next;
        }
        freeifaddrs(addrs);
    }
    
    dataCountDic = @{
                      PWFUtilsWifiSndCount : @(wifiSnd),
                      PWFUtilsWifiRcvCount : @(wifiRcv),
                      PWFUtilsWWanSndCount : @(wwanSnd),
                      PWFUtilsWWanRcvCount : @(wwanRcv),
                    };
    
    return dataCountDic;
}

/**
 获取自开机以来Wifi发送量(以B为单位)

 @return Wifi发送量
 */
+ (NSUInteger)getWifiSndCount {
    NSDictionary *dataCountDic = [self getMoblieDataTraffic];
    
    if (dataCountDic == nil) {
        return 0;
    }
    
    return [dataCountDic[PWFUtilsWifiSndCount] integerValue];
}

/**
 获取自开机以来Wifi接收量(以B为单位)

 @return Wifi接收量
 */
+ (NSUInteger)getWifiRcvCount {
    NSDictionary *dataCountDic = [self getMoblieDataTraffic];
    
    if (dataCountDic == nil) {
        return 0;
    }
    
    return [dataCountDic[PWFUtilsWifiRcvCount] integerValue];
}

/**
 获取自开机以来移动网络的发送量(以B为单位)

 @return 移动网络的发送量
 */
+ (NSUInteger)getWWanSndCount {
    NSDictionary *dataCountDic = [self getMoblieDataTraffic];
    
    if (dataCountDic == nil) {
        return 0;
    }
    
    return [dataCountDic[PWFUtilsWWanSndCount] integerValue];
}

/**
 获取自开机以来移动网络的接收量(以B为单位)

 @return 移动网络的接收量
 */
+ (NSUInteger)getWWanRcvCount {
    NSDictionary *dataCountDic = [self getMoblieDataTraffic];
    
    if (dataCountDic == nil) {
        return 0;
    }
    
    return [dataCountDic[PWFUtilsWWanRcvCount] integerValue];
}

/**
 获取设备总的内存大小(以MB为单位)

 @return 总内存大小
 */
+ (CGFloat)totalMemory
{
    return [NSProcessInfo processInfo].physicalMemory / 1024.f / 1024.f;
}

/**
 获取设备的可用内存大小(以MB为单位)

 @return 设备可用内存大小
 */
+ (CGFloat)freeMemory
{
    vm_statistics_data_t vmStats;
    mach_msg_type_number_t infoCount = HOST_VM_INFO_COUNT;
    kern_return_t kernReturn = host_statistics(mach_host_self(),
                                               HOST_VM_INFO,
                                               (host_info_t)&vmStats,
                                               &infoCount);
    
    if (kernReturn != KERN_SUCCESS) {
        return NSNotFound;
    }
    
    return (vm_page_size * vmStats.free_count + vm_page_size * vmStats.inactive_count) / 1024.0 / 1024.0;
}

/**
 CPU使用百分比

 @return 返回CPU使用百分比
 */
+ (CGFloat)cpuUsagePercent
{
    kern_return_t kr;
    task_info_data_t tinfo;
    mach_msg_type_number_t task_info_count;
    
    task_info_count = TASK_INFO_MAX;
    kr = task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)tinfo, &task_info_count);
    if (kr != KERN_SUCCESS) {
        return -1;
    }
    
    task_basic_info_t      basic_info;
    thread_array_t         thread_list;
    mach_msg_type_number_t thread_count;
    
    thread_info_data_t     thinfo;
    mach_msg_type_number_t thread_info_count;
    
    thread_basic_info_t basic_info_th;
    uint32_t stat_thread = 0; // Mach threads
    
    basic_info = (task_basic_info_t)tinfo;
    
    // get threads in the task
    kr = task_threads(mach_task_self(), &thread_list, &thread_count);
    if (kr != KERN_SUCCESS) {
        return -1;
    }
    if (thread_count > 0)
        stat_thread += thread_count;
    
    long tot_sec = 0;
    long tot_usec = 0;
    float tot_cpu = 0;
    int j;
    
    for (j = 0; j < thread_count; j++)
    {
        thread_info_count = THREAD_INFO_MAX;
        kr = thread_info(thread_list[j], THREAD_BASIC_INFO,
                         (thread_info_t)thinfo, &thread_info_count);
        if (kr != KERN_SUCCESS) {
            return -1;
        }
        
        basic_info_th = (thread_basic_info_t)thinfo;
        
        if (!(basic_info_th->flags & TH_FLAGS_IDLE)) {
            tot_sec = tot_sec + basic_info_th->user_time.seconds + basic_info_th->system_time.seconds;
            tot_usec = tot_usec + basic_info_th->system_time.microseconds + basic_info_th->system_time.microseconds;
            tot_cpu = tot_cpu + basic_info_th->cpu_usage / (float)TH_USAGE_SCALE * 100.0;
        }
        
    } // for each thread
    
    kr = vm_deallocate(mach_task_self(), (vm_offset_t)thread_list, thread_count * sizeof(thread_t));
    assert(kr == KERN_SUCCESS);
    
    return tot_cpu;
}

/**
 获取设备的可用存储空间(以MB为单位)

 @return 设备可用的存储空间大小
 */
+ (CGFloat)freeDiskSize
{
    struct statfs buf;
    unsigned long long freeSpace = -1;
    if (statfs("/var", &buf) >= 0)
    {
        freeSpace = (unsigned long long)(buf.f_bsize * buf.f_bavail);
    }
    
    return freeSpace / 1024.0 / 1024.0;
}

/**
 获取设备的可用存储空间(以MB为单位)
 
 @return 设备可用的存储空间大小
 */
+ (CGFloat)freeDiskSize1
{
    CGFloat size = 0.0;
    NSError *error;
    NSDictionary *dic = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:&error];
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
    }else{
        NSNumber *number = [dic objectForKey:NSFileSystemFreeSize];
        size = [number floatValue] / 1024 / 1024;
    }
    
    return size;
}

/**
 获取磁盘总的空间大小(MB为单位)
 
 @return 返回磁盘总大小
 */
+ (CGFloat)totalDiskSize
{
    struct statfs buf;
    unsigned long long freeSpace = -1;
    if (statfs("/var", &buf) >= 0)
    {
        freeSpace = (unsigned long long)(buf.f_bsize * buf.f_blocks);
    }
    
    return freeSpace / 1024 / 1024;
}

/**
 获取磁盘总的空间大小(MB为单位)

 @return 返回磁盘总大小
 */
+ (CGFloat)totalDiskSize1
{
    CGFloat size = 0.0;
    NSError *error;
    NSDictionary *dic = [[NSFileManager defaultManager] attributesOfFileSystemForPath:NSHomeDirectory() error:&error];
    if (error) {
        NSLog(@"error: %@", error.localizedDescription);
    }else{
        NSNumber *number = [dic objectForKey:NSFileSystemSize];
        size = [number floatValue] / 1024 / 1024;
    }
    
    return size;
}

/**
 获取电池电量

 @return 电池电量
 */
+ (CGFloat)getBatteryQuantity
{
    return [[UIDevice currentDevice] batteryLevel];
}

/**
 获取系统版本

 @return 系统当前版本号
 */
+ (CGFloat)systemVersion
{
    NSString *osVersion = [[UIDevice currentDevice] systemVersion];
    return [osVersion floatValue];
}

/**
 系统当前版本号是否大于11.0

 @return YES:是
 */
+ (BOOL)systemVersionUpper11
{
    return [self systemVersion] > 11.0;
}
@end
