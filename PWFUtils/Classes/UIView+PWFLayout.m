//
//  UIView+WFUtils.m
//  WFUtils
//
//  Created by pengweifeng on 2017/6/8.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import "UIView+PWFLayout.h"

@implementation UIView (PWFUtils)

/**
 返回view的上端距父视图的距离

 @return view的上端距父视图的距离
 */
- (CGFloat)top
{
    return CGRectGetMinY(self.frame);
}

- (void)setTop:(CGFloat)top
{
    CGRect vFrame = self.frame;
    vFrame.origin.y = top;
    
    self.frame = vFrame;
}

- (CGFloat)left
{
    return CGRectGetMinX(self.frame);
}

- (void)setLeft:(CGFloat)left
{
    CGRect vFrame = self.frame;
    vFrame.origin.x = left;
    
    self.frame = vFrame;
}

- (CGFloat)bottom
{
    return CGRectGetMaxY(self.frame);
}

- (void)setBottom:(CGFloat)bottom
{
    CGRect vFrame = self.frame;
    vFrame.origin.y = bottom - CGRectGetHeight(self.frame);
    
    self.frame = vFrame;
}

- (CGFloat)right
{
    return CGRectGetMaxX(self.frame);
}

- (void)setRight:(CGFloat)right
{
    CGRect vFrame = self.frame;
    vFrame.origin.x = right - CGRectGetWidth(self.frame);
    self.frame = vFrame;
}

- (CGFloat)width
{
    return CGRectGetWidth(self.frame);
}

- (void)setWidth:(CGFloat)width
{
    CGRect vFrame = self.frame;
    vFrame.size.width = width;
    
    self.frame = vFrame;
}

- (CGFloat)height
{
    return CGRectGetHeight(self.frame);
}

- (void)setHeight:(CGFloat)height
{
    CGRect vFrame = self.frame;
    vFrame.size.height = height;
    
    self.frame = vFrame;
}

- (CGFloat)centerX
{
    return CGRectGetMidX(self.frame);
}

- (void)setCenterX:(CGFloat)centerX
{
    CGPoint center = self.center;
    center.x = centerX;
    
    self.center = center;
}

- (CGFloat)centerY
{
    return CGRectGetMidY(self.frame);
}

- (void)setCenterY:(CGFloat)centerY
{
    CGPoint center = self.center;
    center.y = centerY;
    
    self.center = center;
}

- (CGSize)size
{
    return self.frame.size;
}

- (void)setSize:(CGSize)size
{
    CGRect vFrame = self.frame;
    vFrame.size = size;
    
    self.frame = vFrame;
}

- (CGPoint)origin
{
    return self.frame.origin;
}

- (void)setOrigin:(CGPoint)origin
{
    CGRect vFrame = self.frame;
    vFrame.origin = origin;
    
    self.frame = vFrame;
}
@end
