//
//  NSString+WFUtils.m
//  WFUtils
//
//  Created by pengweifeng on 2017/6/1.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import "NSString+PWFUtils.h"
#import <CommonCrypto/CommonDigest.h>

@implementation NSString (PWFUtils)

/**
 根据文本的字体和最大宽度计算文本占用的大小

 @param font 文本的字体大小
 @param maxWidth 文本最大的宽度
 @return 文本适配后的大小
 */
- (CGSize)sizeWithFont:(CGFloat)font maxWidth:(CGFloat)maxWidth
{
    if (self.length == 0) {
        return CGSizeZero;
    }
    
    NSDictionary *attributesDict = @{ NSFontAttributeName:[UIFont systemFontOfSize:font] };
    CGSize maxSize = CGSizeMake(maxWidth, MAXFLOAT);

    CGRect subviewRect = [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesDict context:nil];
    
    return CGSizeMake(ceil(subviewRect.size.width), ceil(subviewRect.size.height));
}

/**
 根据文本的字体和最大高度计算文本占用的大小

 @param font 文本的字体
 @param maxHeight 文本最大高度
 @return 文本适配后的大小
 */
- (CGSize)sizeWithFont:(CGFloat)font maxHeight:(CGFloat)maxHeight
{
    if (self.length == 0) {
        return CGSizeZero;
    }
    
    NSDictionary *attributesDict = @{ NSFontAttributeName:[UIFont systemFontOfSize:font] };
    CGSize maxSize = CGSizeMake(MAXFLOAT, maxHeight);
    
    CGRect subviewRect = [self boundingRectWithSize:maxSize options:NSStringDrawingUsesLineFragmentOrigin attributes:attributesDict context:nil];
    
    return CGSizeMake(ceil(subviewRect.size.width), ceil(subviewRect.size.height));
}

/**
 把字符串解析成字典

 @return 转换后的字典
 */
- (NSDictionary *)toDictionary
{
    if (self.length == 0) {
        return nil;
    }
    
    NSData *jsonData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&err];
    
    if (err != nil) {
        return nil;
    } else {
        return dic;
    }
}

/**
 获取string的MD5值

 @return 返回string加密后的MD5值
 */
- (NSString *)MD5 {
    const char *concat_str = [self UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(concat_str, (unsigned int)strlen(concat_str), result);
    NSMutableString *hash = [NSMutableString string];
    for (int i = 0; i < 16; i++){
        [hash appendFormat:@"X", result[i]];
    }
    return [hash uppercaseString];
}

/**
 base64加密

 @return base64加密后的字符串
 */
- (NSString *)base64EncodedString {
    NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
    
    return [data base64EncodedStringWithOptions:0];
}

/**
 base64解密

 @return base64解密后的字符串
 */
- (NSString *)base64DecodedString {
    NSData *data = [[NSData alloc] initWithBase64EncodedString:self options:0];
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

/**
 判断字符串是否全是数字

 @return YES:是,NO:不是
 */
- (BOOL)isAllNum {
    if (self.length == 0) {
        return NO;
    }
    
    unichar character;
    for (int i = 0; i < self.length; i++) {
        character = [self characterAtIndex:i];
        if (!isdigit(character)) {
            return NO;
        }
    }
    
    return YES;
}

/**
 是否有效的Email

 @return YES:是
 */
- (BOOL)isValidEmail
{
    if (self.length == 0) {
        return NO;
    }
    
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:self];
}

/**
 是否有效的电话号码

 @return YES:是
 */
- (BOOL)isValidPhoneNumber
{
    NSString *phoneNumber = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    if (phoneNumber.length != 11) {
        return NO;
    }
    
    BOOL isMatchMobile = [self isChinaMobile];
    BOOL isMatchUnicom = [self isChinaUnicom];
    BOOL isMatchTelecom = [self isChinaTelecom];
    
    return (isMatchMobile || isMatchTelecom || isMatchUnicom);
}

/**
 是否移动号
 
 @return YES:是
 */
- (BOOL)isChinaMobile
{
    //移动号段有  1(34|35|36|37|38|39|47|48|50|51|52|57|58|59|78|82|83|84|87|88|98)
    //170[3|5|6] ，170第四位是3或5或6的；
    
    if (self.length == 0) {
        return NO;
    }
    
    NSString *chinaMobileReg = @"^((13[4-9])|(147)|(148)|(15[0-2,7-9])|(178)|(18[2-4,7-8])|(198))\\d{8}$|^(170[3,5,6])\\d{7}$";
    NSPredicate *regexTestMobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", chinaMobileReg];
    
    return [regexTestMobile evaluateWithObject:self];
}

/**
 是否联通号
 
 @return YES:是
 */
- (BOOL)isChinaUnicom
{
    //联通号段有  1(30|31|32|45|46|55|56|71|75|76|85|86|66)，170[4|7|8|9]
    if (self.length == 0) {
        return NO;
    }
    
    NSString *chinaUnicomReg = @"^((13[0-2])|(14[5-6])|(15[5-6])|(171)|(17[5-6])|(18[5,6])|(166))\\d{8}$|^(170[4,7,8,9])\\d{7}$";
    NSPredicate *regexTestUnicom = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", chinaUnicomReg];
    
    return [regexTestUnicom evaluateWithObject:self];
}

/**
 是否中国电信号
 
 @return YES:是
 */
- (BOOL)isChinaTelecom
{
    //    电信号段有  1(33|80|81|89|53|73|77|99) 170[0|1|2] 149[0-9] 174[0|1]
    if (self.length == 0) {
        return NO;
    }
    
    NSString *chinaTelecomReg = @"^((133)|(18[0,1,9])|(153)|(17[3,7])|(199))\\d{8}$|^((170[0,1,2])|(149[0-9])|(174[0-1]))\\d{7}$";
    NSPredicate *regexTestTelecom = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", chinaTelecomReg];
    
    return [regexTestTelecom evaluateWithObject:self];
}

/** 比较两个版本号的大小
 @param v1 第一个版本号
 @param v2 第二个版本号
 @return 版本号相等,返回0; v1小于v2,返回-1; 否则返回1.
 */
+ (NSInteger)compareVersion:(NSString *)v1 to:(NSString *)v2
{
    // 都为空，相等，返回0
    if (!v1 && !v2) {
        return 0;
    }
    
    //v1为空，v2不为空，返回-1
    if (!v1 && v2) {
        return -1;
    }
    
    //v2为空，v1不为空，返回1
    if (v1 && !v2) {
        return 1;
    }
    
    // 获取版本号字段
    NSArray *v1Array = [v1 componentsSeparatedByString:@"."];
    NSArray *v2Array = [v2 componentsSeparatedByString:@"."];
    
    // 取字段最少的，进行循环比较
    NSInteger smallCount = (v1Array.count > v2Array.count) ? v2Array.count : v1Array.count;
    for (int i = 0; i < smallCount; i++) {
        NSInteger value1 = [[v1Array objectAtIndex:i] integerValue];
        NSInteger value2 = [[v2Array objectAtIndex:i] integerValue];
        if (value1 > value2) {
            //v1版本字段大于v2版本字段，返回1
            return 1;
        } else if (value1 < value2) {
            // v2版本字段大于v1版本字段，返回-1
            return -1;
        } // 版本相等，继续循环。
    }
    
    // 版本可比较字段相等，则字段多的版本高于字段少的版本。
    if (v1Array.count > v2Array.count) {
        return 1;
    } else if (v1Array.count < v2Array.count) {
        return -1;
    } else {
        return 0;
    }
}
@end
