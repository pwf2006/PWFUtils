//
//  UIColor+WFUtils.h
//  WFUtils
//
//  Created by pengweifeng on 2017/6/2.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PWFUtils)

/**
 根据十六进制整数及不透明度算出对应的颜色
 
 @param hexValue 十六进制整数
 @param alphaValue 不透明度
 @return 对应的颜色对象
 */
+ (UIColor *)colorWithHex:(NSUInteger)hexValue alpha:(CGFloat)alphaValue;

/**
 根据十六进制整数算出对应的颜色
 
 @param hexValue 十六进制整数
 @return 对应的颜色对象
 */
+ (UIColor *)colorWithHex:(NSUInteger)hexValue;

/**
 根据十六进制字符串算出对应的颜色
 
 @param colorStr 十六进制字符串
 @return 对应的颜色对象
 */
+ (UIColor *)colorWithHexString:(NSString *)colorStr;

/**
 获取一个随机色
 
 @return 返回一个生成的随机色
 */
+ (UIColor *)randomColor;

@end
