//
//  UIControl+PWFUtils.m
//  PWFUtils
//
//  Created by pengweifeng on 2017/9/30.
//  Copyright © 2017年 pwf2006. All rights reserved.
//

#import "UIControl+PWFUtils.h"
#import <objc/runtime.h>

@implementation UIControl (PWFUtils)

- (void)setClickInterval:(NSTimeInterval)aClickInterval
{
    objc_setAssociatedObject(self, @selector(clickInterval), @(aClickInterval), OBJC_ASSOCIATION_ASSIGN);
}

- (NSTimeInterval)clickInterval
{
    NSNumber *clickIntervalNumber = objc_getAssociatedObject(self, @selector(clickInterval));
    return [clickIntervalNumber doubleValue];
}

- (void)setCanNotClick:(BOOL)aCanNotClick
{
    objc_setAssociatedObject(self, @selector(canNotClick), @(aCanNotClick), OBJC_ASSOCIATION_ASSIGN);
}

- (BOOL)canNotClick
{
    NSNumber *canNotClickNumber = objc_getAssociatedObject(self, @selector(canNotClick));
    return [canNotClickNumber boolValue];
}

+ (void)load
{
    Method rawMethod = class_getInstanceMethod(self, @selector(sendAction:to:forEvent:));
    Method replaceMethod = class_getInstanceMethod(self, @selector(pwf_sendAction:to:forEvent:));
    method_exchangeImplementations(rawMethod, replaceMethod);
}

- (void)pwf_sendAction:(SEL)action to:(id)target forEvent:(UIEvent *)event
{
    if (self.canNotClick == YES) {
        return;
    }
    
    [self pwf_sendAction:action to:target forEvent:event];
    
    if (self.clickInterval > 0) {
        self.canNotClick = YES;
        [self performSelector:@selector(setCanNotClick:) withObject:@(NO) afterDelay:self.clickInterval];
    }
}
@end
