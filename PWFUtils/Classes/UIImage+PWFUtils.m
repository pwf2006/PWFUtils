//
//  UIImage+PWFUtils.m
//  Pods
//
//  Created by pengweifeng on 2017/7/13.
//
//

#import "UIImage+PWFUtils.h"

@implementation UIImage (PWFUtils)

/**
 根据颜色获取图片

 @param color 颜色
 @return 根据颜色生成的图片
 */
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

/**
 png图片转二进制数据

 @return 二进制数据
 */
- (NSData *)pngToData
{
    if (self == nil) {
        return nil;
    }
    
    NSData *imageData = UIImagePNGRepresentation(self);
    
    return imageData;
}

/**
 jpg图片转二进制数据

 @return 二进制数据
 */
- (NSData *)jpgToData
{
    if (self == nil) {
        return nil;
    }
    
    NSData *imgData = UIImageJPEGRepresentation(self, 1);
    
    return imgData;
}

/**
 根据图片的二进制数据判断图片的类型

 @param data 图片的二进制数据
 @return 图片的类型
 */
+ (NSString *)typeWithData:(NSData *)data
{
    if (data.length == 0) {
        return nil;
    }
    
    uint8_t type;
    [data getBytes:&type length:1];
    
    NSString *typeStr;
    switch (type) {
        case 0xFF:
            typeStr = @"image/jpeg";
            break;
            
        case 0x89:
            typeStr = @"image/png";
            break;
            
        case 0x47:
            typeStr = @"image/gif";
            break;
            
        case 0x49:
            
        case 0x4D:
            typeStr = @"image/tiff";
            break;
            
        default:
            break;
    }
    
    return typeStr;
}
@end
