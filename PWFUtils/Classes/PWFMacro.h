//
//  WFMacro.h
//  WFUtils
//
//  Created by pengweifeng on 2017/6/8.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#ifndef PWFMacro_h
#define PWFMacro_h

#define SCREEN_WIDTH CGRectGetWidth([UIScreen mainScreen].bounds)
#define SCREEN_HEIGHT CGRectGetHeight([UIScreen mainScreen].bounds)

#define PWFWeakSelf(type)  __weak typeof(type) weak##type = type
#define PWFStrongSelf(type)  __strong typeof(type) type = weak##type

#define PWFDegreesToRadian(x) (M_PI * (x) / 180.0)
#define PWFRadianToDegrees(radian) (radian * 180.0)/(M_PI)

#endif /* WFMacro_h */
