//
//  NSString+WFUtils.h
//  WFUtils
//
//  Created by pengweifeng on 2017/6/1.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (PWFUtils)

/** 比较两个版本号的大小
 @param v1 第一个版本号
 @param v2 第二个版本号
 @return 版本号相等,返回0; v1小于v2,返回-1; 否则返回1.
 */
+ (NSInteger)compareVersion:(NSString *)v1 to:(NSString *)v2;

/**
 根据文本的字体和最大宽度计算文本占用的大小
 
 @param font 文本的字体大小
 @param maxWidth 文本最大的宽度
 @return 文本适配后的大小
 */
- (CGSize)sizeWithFont:(CGFloat)font maxWidth:(CGFloat)maxWidth;

/**
 根据文本的字体和最大高度计算文本占用的大小
 
 @param font 文本的字体
 @param maxHeight 文本最大高度
 @return 文本适配后的大小
 */
- (CGSize)sizeWithFont:(CGFloat)font maxHeight:(CGFloat)maxHeight;

/**
 把字符串解析成字典
 
 @return 转换后的字典
 */
- (NSDictionary *)toDictionary;

/**
 获取string的MD5值
 
 @return 返回string加密后的MD5值
 */
- (NSString *)MD5;

/**
 base64加密
 
 @return base64加密后的字符串
 */
- (NSString *)base64EncodedString;

/**
 base64解密
 
 @return base64解密后的字符串
 */
- (NSString *)base64DecodedString;

/**
 判断字符串是否全是数字
 
 @return YES:是,NO:不是
 */
- (BOOL)isAllNum;

/**
 是否有效的Email
 
 @return YES:是
 */
- (BOOL)isValidEmail;

/**
 是否有效的电话号码
 
 @return YES:是
 */
- (BOOL)isValidPhoneNumber;

/**
 是否移动号
 
 @return YES:是
 */
- (BOOL)isChinaMobile;

/**
 是否联通号
 
 @return YES:是
 */
- (BOOL)isChinaUnicom;

/**
 是否中国电信号
 
 @return YES:是
 */
- (BOOL)isChinaTelecom;

@end
