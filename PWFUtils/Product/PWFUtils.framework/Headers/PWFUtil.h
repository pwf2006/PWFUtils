
//
//  WFUtils.h
//  WFUtils
//
//  Created by pengweifeng on 2017/6/1.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PWFUtil : NSObject

/**
 获取UUID
 
 @return 返回生成的UUID
 */
+ (NSString *)UUID;

/**
 获取设备信息
 
 @return 返回设备信息
 */
+ (NSString *)getDeviceInfo;

/**
 获取app名称
 
 @return 返回app名称
 */
+ (NSString *)appName;

/**
 获取app版本号
 
 @return 返回app版本号
 */
+ (NSString *)appVersion;

/**
 获取app build号
 
 @return 返回app build号
 */
+ (NSString *)appBuidVersion;

/**
 获取自上次开机以来wifi和wwan各自的收发总量(以B为单位)
 
 @return 包含wifi和wwan各自收发总量的字典
 */
+ (NSDictionary *)getMoblieDataTraffic;

/**
 获取自开机以来Wifi发送量(以B为单位)
 
 @return Wifi发送量
 */
+ (NSUInteger)getWifiSndCount;

/**
 获取自开机以来Wifi接收量(以B为单位)
 
 @return Wifi接收量
 */
+ (NSUInteger)getWifiRcvCount;

/**
 获取自开机以来移动网络的发送量(以B为单位)
 
 @return 移动网络的发送量
 */
+ (NSUInteger)getWWanSndCount;

/**
 获取自开机以来移动网络的接收量(以B为单位)
 
 @return 移动网络的接收量
 */
+ (NSUInteger)getWWanRcvCount;

/**
 获取设备总的内存大小(以MB为单位)
 
 @return 总内存大小
 */
+ (CGFloat)totalMemory;

/**
 获取设备的可用内存大小(以MB为单位)
 
 @return 设备可用内存大小
 */
+ (CGFloat)freeMemory;

/**
 CPU使用百分比
 
 @return 返回CPU使用百分比
 */
+ (CGFloat)cpuUsagePercent;

/**
 获取设备的可用存储空间(以MB为单位)
 
 @return 设备可用的存储空间大小
 */
+ (CGFloat)freeDiskSize;

/**
 获取磁盘总的空间大小(MB为单位)
 
 @return 返回磁盘总大小
 */
+ (CGFloat)totalDiskSize;

/**
 获取电池电量
 
 @return 电池电量
 */
+ (CGFloat)getBatteryQuantity;

@end
