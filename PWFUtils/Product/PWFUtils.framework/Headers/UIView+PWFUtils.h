//
//  UIView+WFUtils.h
//  WFUtils
//
//  Created by pengweifeng on 2017/6/8.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//
//  view布局的扩展(*分类不会增加类的属性,这里只是添加的getter,setter方法

#import <UIKit/UIKit.h>

@interface UIView (WFUtils)

@property (nonatomic, assign) CGFloat top;

@property (nonatomic, assign) CGFloat left;

@property (nonatomic, assign) CGFloat bottom;

@property (nonatomic, assign) CGFloat right;

@property (nonatomic, assign) CGFloat width;

@property (nonatomic, assign) CGFloat height;

@property (nonatomic, assign) CGFloat centerX;

@property (nonatomic, assign) CGFloat centerY;

@property (nonatomic, assign) CGSize size;

@property (nonatomic, assign) CGPoint origin;

@end
