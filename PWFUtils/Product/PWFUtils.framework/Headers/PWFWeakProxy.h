//
//  PWFWeakProxy.h
//  PWFUtils
//
//  Created by pengweifeng on 2017/12/7.
//

#import <Foundation/Foundation.h>

@interface PWFWeakProxy : NSProxy

@property (nullable, nonatomic, weak, readonly) id target;

- (_Nullable instancetype)initWithTarget:(_Nonnull id)target;

+ (_Nullable instancetype)proxyWithTarget:(_Nonnull id)target;

@end
