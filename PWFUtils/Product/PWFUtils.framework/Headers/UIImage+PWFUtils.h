//
//  UIImage+PWFUtils.h
//  Pods
//
//  Created by pengweifeng on 2017/7/13.
//
//

#import <UIKit/UIKit.h>

@interface UIImage (PWFUtils)

/**
 根据颜色获取图片
 
 @param color 颜色
 @return 根据颜色生成的图片
 */
+ (UIImage *)imageWithColor:(UIColor *)color;

/**
 png图片转二进制数据
 
 @return 二进制数据
 */
- (NSData *)pngToData;

/**
 jpg图片转二进制数据
 
 @return 二进制数据
 */
- (NSData *)jpgToData;

/**
 根据图片的二进制数据判断图片的类型
 
 @param data 图片的二进制数据
 @return 图片的类型
 */
+ (NSString *)typeWithData:(NSData *)data;

@end
