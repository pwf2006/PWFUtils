//
//  NSAttributedString+WFUtils.h
//  WFUtils
//
//  Created by pengweifeng on 2017/6/1.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSAttributedString (PWFUtils)

/**
 根据宽度计算属性字符串的高度
 
 @param width 属性字符串要占用的宽度
 @return 属性字符串匹配后的高度
 */
- (CGFloat)heightForWidth:(CGFloat)width;

@end
