//
//  NSData+WFUtils.h
//  WFUtils
//
//  Created by pengweifeng on 2017/6/1.
//  Copyright © 2017年 pengweifeng. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSData (PWFUtils)

/**
 gzip解压缩
 
 @return 解压之后的数据
 */
- (NSData *)uncompressZippedData;

/**
 gzip压缩文件
 
 @return gzip压缩之后的数据
 */
- (NSData *)gzipData;

/**
 Data->对象
 
 @return 对象
 */
- (id)parseData;

/**
 *  利用AES加密数据
 */
- (NSData*)encryptedWithAESUsingKey:(NSString*)key andIV:(NSData*)iv;

/**
 *  利用AES解密据
 */
- (NSData*)decryptedWithAESUsingKey:(NSString*)key andIV:(NSData*)iv;

/**
 *  利用3DES加密数据
 */
- (NSData*)encryptedWith3DESUsingKey:(NSString*)key andIV:(NSData*)iv;

/**
 *  利用3DES解密数据
 */
- (NSData*)decryptedWith3DESUsingKey:(NSString*)key andIV:(NSData*)iv;

/**
 NSData->Base64 string
 
 @return Base64 string
 */
- (NSString *)base64EncodeString;

/**
 Base64 string -> NSData
 
 @param encodedBase64Str Base64 string
 @return 转化后的NSData
 */
+ (NSData *)decodeBase64String:(NSString *)encodedBase64Str;

@end
