# PWFUtils

[![CI Status](http://img.shields.io/travis/pwf2006/PWFUtils.svg?style=flat)](https://travis-ci.org/pwf2006/PWFUtils)
[![Version](https://img.shields.io/cocoapods/v/PWFUtils.svg?style=flat)](http://cocoapods.org/pods/PWFUtils)
[![License](https://img.shields.io/cocoapods/l/PWFUtils.svg?style=flat)](http://cocoapods.org/pods/PWFUtils)
[![Platform](https://img.shields.io/cocoapods/p/PWFUtils.svg?style=flat)](http://cocoapods.org/pods/PWFUtils)
   
PWFUtils是iOS日常开发中一些基础类的辅助开发,工程引入该pod可以提高开发人员的开发效率,支持以源码和Framework两种方式引入工程.
## Example

To run the example project, clone the repo,if you want to reference the framework, then run `pod install` from the Example directory;If you want to reference the source code, run `IS_SOURCE=1 pod install`.

## Requirements

## Installation

PWFUtils is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "PWFUtils"
```

## Author

pwf2006, 674423263@qq.com

## License

PWFUtils is available under the MIT license. See the LICENSE file for more info.
