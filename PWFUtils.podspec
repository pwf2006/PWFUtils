#
# Be sure to run `pod lib lint PWFUtils.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'PWFUtils'
  s.version          = '0.1.9'
  s.summary          = 'A serial of classes to enhance the iOS developers work efficiency.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
    该库把一些常用的工具方法集中起来,方便iOS开发者提高开发效率.对一些基础类,我们以category的方式提供便捷方法.后续会根据需要陆续再加进去一些其它方法.
    0.1.3:View+PWFUtils setRight方法的bug,以前的一些头文件没打到framework中;
    0.1.4:去掉framework下载的支持,不想升级cocoapod以免影响公司项目cocoapod使用;
    0.1.5:NSString+PWFUtils 类增加App版本号的比较函数;
    0.1.6:增加获取系统版本号的方法,和判断当前系统版本号是否大于11.0;
    0.1.7:增加UIView+PWFAlert扩展,UIView+PWFUtil.h->UIView+PWFLayout.h
    0.1.8:增加对字符串判断是移动号码,联通号码,电信号码的判断;
    0.1.9:增加UITextField+PWFPhoneNumber对手机344分隔的扩展;
                       DESC

  s.homepage         = 'https://git.oschina.net/pwf2006/PWFUtils'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'pwf2006' => '674423263@qq.com' }
  s.source           = { :git => 'https://git.oschina.net/pwf2006/PWFUtils.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'
  s.requires_arc = true

#if ENV['IS_SOURCE']
    puts '---------------------------'
    puts 'PWFUtils 使用源码'
    puts '---------------------------'
    s.source_files = 'PWFUtils/Classes/**/*'
#else
#  puts '---------------------------'
#  puts 'PWFUtils 使用Framework'
# puts '---------------------------'
# s.ios.vendored_frameworks = 'PWFUtils/Product/PWFUtils.framework'
# end

  s.library = 'z'
  
  # s.resource_bundles = {
  #   'PWFUtils' => ['PWFUtils/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  s.dependency 'MBProgressHUD', '1.1.0'
end
