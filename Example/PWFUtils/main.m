//
//  main.m
//  PWFUtils
//
//  Created by pwf2006 on 07/13/2017.
//  Copyright (c) 2017 pwf2006. All rights reserved.
//

@import UIKit;
#import "PWFAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([PWFAppDelegate class]));
    }
}
