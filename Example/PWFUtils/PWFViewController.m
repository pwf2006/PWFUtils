//
//  PWFViewController.m
//  PWFUtils
//
//  Created by pwf2006 on 07/13/2017.
//  Copyright (c) 2017 pwf2006. All rights reserved.
//

#import "PWFViewController.h"
#import <PWFUtils/PWFUtil.h>

@interface PWFViewController ()

@end

@implementation PWFViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSLog(@"*** wifisend:%@", @([PWFUtil getWifiSndCount] / 1024.f / 1024.f));
    NSLog(@"*** wifireceive:%@", @([PWFUtil getWifiRcvCount] / 1024.f / 1024.f));
    NSLog(@"*** wwansend:%@", @([PWFUtil getWWanSndCount] / 1024.f / 1024.f));
    NSLog(@"*** wwanreceive:%@",@([PWFUtil getWWanRcvCount] / 1024.f / 1024.f));
    NSLog(@"*** cpu use percent:%@", @([PWFUtil cpuUsagePercent]));
    NSLog(@"*** free memory:%@", @([PWFUtil freeMemory]));
    NSLog(@"*** total memory:%@", @([PWFUtil totalMemory]));
    NSLog(@"*** free disk size:%@", @([PWFUtil freeDiskSize]));
    NSLog(@"*** total disk size:%@", @([PWFUtil totalDiskSize]));
    NSLog(@"*** battery quatity:%@", @([PWFUtil getBatteryQuantity]));
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
