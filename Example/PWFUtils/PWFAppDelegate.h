//
//  PWFAppDelegate.h
//  PWFUtils
//
//  Created by pwf2006 on 07/13/2017.
//  Copyright (c) 2017 pwf2006. All rights reserved.
//

@import UIKit;

@interface PWFAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
